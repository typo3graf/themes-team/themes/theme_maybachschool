<?php

/**
 * Extension Manager/Repository config file for ext "theme_maybachschool".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Theme: Maybach School',
    'description' => 'Theme template for school homepages.',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'bootstrap_package' => '11.0.0-11.0.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Typo3graf\\ThemeMaybachschool\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'T3graf Design-Team',
    'author_email' => 'support@typo3graf.de',
    'author_company' => 'Typo3graf media-agentur UG',
    'version' => '1.0.0',
];
